var isOpenNav=false;

//คลิกปุ่ม toggle menu
$('.navbar-toggler').on('click',()=>{
  isOpenNav = !isOpenNav

  if(isOpenNav){
    //เปิด Nav
    $('.sidebar').addClass('show')
    onOverlay()
  }else{
    //ปิดNav
    $('.sidebar').removeClass('show')
    offOverlay()
  }

  // $('.navbar-toggler').toggleClass('open')
})


//Task:  Side bar เปิด-ปิดปุ่ม Sub-menu
document.querySelectorAll('.sidebar .nav-link').forEach((element)=>{
  //element = tag a ในแต่ละตัว

  //ถ้า tag a ตัวไหนถูกคลิก
  element.addEventListener('click',(e)=>{
    // div class = submenu collapse 
    let childEl = element.nextElementSibling;
    // li 
    let parentEl = element.parentElement;
    
    //ถ้ามีตัวลูก (มี div)
    if(childEl){
      e.preventDefault()
      let mycollapse = new bootstrap.Collapse(childEl);
      if(childEl.classList.contains('show')){
        //ถ้าchild มี class = show อยู่แล้ว
        //ให้ hide ไป
        mycollapse.hide();
      }else{
        //ถ้าchild  ไม่มี class = show แต่จริงๆ show ตั้งแต่บรรทัดที่ 31 แล้ว 
        mycollapse.show();

        //หา other submenus [li] ที่มี class = show (กำลังเปิดอยู่) ให้ทำการปิดซะ ! เพราะเดียวเปิดเยอะเกิ้นน 
        closeOtherSubmenu();
      }
      //rotate ปุ่ม
      $(childEl).siblings('a').toggleClass('rotate')
      //active li
      $(parentEl).toggleClass('active')
    }

    function closeOtherSubmenu() {
      //parentEl = li
      //parentEl.parentElement = ul
      var opened_submenu=$(parentEl).siblings('li.has-submenu').children('.submenu.show')[0]
      //rotate ปุ่ม
      $(parentEl).siblings('li.has-submenu').children('.submenu.show').siblings('a').toggleClass('rotate')
      //ปิด active class
      $(parentEl).siblings('li.has-submenu.active').toggleClass('active')
      if (opened_submenu) {
        new bootstrap.Collapse(opened_submenu);
      }
    }
  })

})


//ปิด sidenav
$('.close-btn').on('click',()=>{
  $('.sidebar').removeClass('show')
  offOverlay()
  isOpenNav = !isOpenNav
})

$('#overlay').on('click',()=>{
  $('.sidebar').removeClass('show')
  offOverlay()
  isOpenNav = !isOpenNav
})


function onOverlay() {
  document.getElementById("overlay").style.display = "block";
}

function offOverlay() {
  document.getElementById("overlay").style.display = "none";
}


//ถ้า มีการ resize หน้าจอให้ปิด nav ไป
addEventListener("resize", (event) => {
  $('.sidebar').removeClass('show')
  offOverlay()
  isOpenNav = !isOpenNav
});