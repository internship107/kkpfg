
const swiperBanner = new Swiper('.swiper-banner', {
  // Optional parameters
  // autoplay:{
    
  //   delay:3000,
  //   disableOnInteraction: false
  // },
  loop:true,
  slidesPerView:1,
  autoHeight:false,
  updateOnWindowResize:true,
  //breakpoint ซะหน่อย
  breakpoints:{
    //>=0
    0:{
    pagination: {
          el: ".swiper-pagination",
          clickable:'true',
          type: "progressbar",
        }
    },
    //>=576
    576:{
    // If we need paginatio
      pagination: {
        el: '.swiper-pagination',
        clickable:'true',
        type:"bullets",
        renderBullet:(index,className)=>{
          return `<span class="${className}"><i></i><b></b></span>`
        }
      },
    }
  },
  // Navigation arrows
  navigation: {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev',
  },
  on:{
    init:function(){
        //เปลื่ยน Init Current Index Pagination
        var displayTotal;
        var total = $('.swiper-banner .swiper-wrapper .swiper-slide').length-2
        if(total<10){
          displayTotal = "0"+total;
        }
        $('#pagination-fraction #total').html(displayTotal)
        $('#pagination-fraction #current').html("01")
    },
    activeIndexChange:function(swiper){
      var displayCurrentIdx;
      if((swiper.realIndex+1)<10){
        displayCurrentIdx = "0"+(swiper.realIndex+1)
      }
      $('#pagination-fraction #current').html(displayCurrentIdx)
    },
    beforeResize:function(swiper){
      swiper.pagination.destroy()
    },
    resize:function(swiper){
      swiper.pagination.init()
      swiper.pagination.render()
      swiper.pagination.update()
    }
  },

  // And if we need scrollbar
  
});


var swiperCard = new Swiper(".swiper-card", {
  spaceBetween: 30,
  loop: true,
  loopFillGroupWithBlank: true,
  slidesPerView: 3,
  slidesPerGroup: 3,
  breakpoints:{
    //>=0
    0:{
      slidesPerView: 1,
      slidesPerGroup: 1,
      // spaceBetween: 15,
    },
    768:{
      slidesPerView: 3,
      slidesPerGroup: 3,
      // spaceBetween: 15,
    },
    1200:{

    }
    
  },
  pagination: {
    el: ".swiper-card .swiper-pagination",
    clickable: true,
  },
  on:{
    beforeResize:function(swiper){
      // swiper.destroy()
    },
    resize:function(swiper){
      // swiper.init()
      // swiper.pagination.render()
      // swiper.pagination.update()
    }
  }

});



var swiper = new Swiper(".swiper-card-expand-mobile", {
  slidesPerView: 1.1,
  spaceBetween: 1,
  centeredSlides: true,
  // freeMode:true,
  // watchSlidesProgress: true,
  // watchSlidesVisibility: true,
  loop:true,
  on:{
    init:function(swiper){
      this.slideTo(2)
    }
  }
});
