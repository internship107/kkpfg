

  //Task: ไม่ปิดถ้ากด dropdown menu (ul)
  function stopPropagationDropdown(){

  $('.navbar .dropdown-menu').on('click', function (e) {
    console.log('Hello dropdown')
    e.stopPropagation();
  });
}

function renderDropDown(){
  //Task:  Side bar เปิด-ปิดปุ่ม Sub-menu
  document.querySelectorAll('.navbar .step2 a.dropdown-toggle').forEach((element)=>{
  //element = tag a ในแต่ละตัว

  //ถ้า tag a ตัวไหนถูกคลิก
  element.addEventListener('click',(e)=>{
    console.log("Hello")

    // div class = submenu collapse  
    let childEl = element.nextElementSibling;

    // li 
    let parentEl = element.parentElement;
    
    //ถ้ามีตัวลูก (มี div)
    if(childEl){
      e.preventDefault()
      let mycollapse = new bootstrap.Collapse(childEl);
      if(childEl.classList.contains('show')){
        //ถ้าchild มี class = show อยู่แล้ว
        //ให้ hide ไป
        mycollapse.hide();
      }else{
        //ถ้าchild  ไม่มี class = show แต่จริงๆ show ตั้งแต่บรรทัดที่ 31 แล้ว 
        mycollapse.show();

        //หา other submenus [li] ที่มี class = show (กำลังเปิดอยู่) ให้ทำการปิดซะ ! เพราะเดียวเปิดเยอะเกิ้นน 
        closeOtherSubmenu();
      }
      //rotate ปุ่ม
      $(childEl).siblings('a').toggleClass('rotate')
      //active li
      $(parentEl).toggleClass('active')
    }

    function closeOtherSubmenu() {
      //parentEl = li
      //parentEl.parentElement = ul
      var opened_submenu=$(parentEl).siblings('li.has-submenu').children('.submenu.show')[0]
      //rotate ปุ่ม
      $(parentEl).siblings('li.has-submenu').children('.submenu.show').siblings('a').toggleClass('rotate')
      //ปิด active class
      $(parentEl).siblings('li.has-submenu.active').toggleClass('active')
      if (opened_submenu) {
        new bootstrap.Collapse(opened_submenu);
      }
    }
  })

})
}

//set .dropdown-menu css to top equal to .navbar-expand-xl height
function setDropdownMenuPosition(){
  console.log('setDropdownMenuPosition')
    document.querySelectorAll('.dropdown-menu').forEach((element)=> {
    element.style.marginTop = 13 + 'px';
    }
  )
}

//set .dropdown-toggle from onclick to hover and will open .dropdown-menu
function setDropdownToggleHover(){
  $("#headerMenu").children(".nav-item").hover(function() {
    $('.step2.show').removeClass('show');
  }, function() {
  });

  $('#headerMenu').children('.dropdown').hover(function() {
      //on mouse over will remove other #headerMenu .dropdown-menu.show 
      $('#headerMenu .dropdown .step2.show').not('.step2 .dropdown').removeClass('show');

      $(this).children('.step2').addClass('show');
  }, function() {
      if (!$(this).is(':hover') && !$(this).children('ul').is(':hover') && !$(this).closest('.navbar').is(':hover')) {
        $(this).children('ul').removeClass('show');
      }

    
  });

};


export {renderDropDown,stopPropagationDropdown,setDropdownMenuPosition,setDropdownToggleHover}
  



