//write event resize
$(window).resize(function() { 

  //removeClass show at class sidebar
  $('.sidebar').removeClass('show')
  //removeClass openSidebar at body
  $('body').removeClass('openSidebar')

});


//write event scroll
$(window).scroll(function() {
  //check height of .navbar
  if ($('.navbar').height() < $(window).scrollTop()) {
    $('.navbar').addClass('fixed-top')
  } else {
    $('.navbar').removeClass('fixed-top')
  }

});


//if hover children #headerMenu > .nav-link will add class active

export function hoverNavLink(){
  $("#headerMenu").children(".nav-item").hover(function() {
    // $(this).children(".nav-link").addClass("active");
  }, function() {
    // $(this).children(".nav-link").removeClass("active");
  });
}

