export function controlSideNav(){
  //Task:  Side bar เปิด-ปิดปุ่ม Sub-menu
  document.querySelectorAll('.sidebar .nav-link').forEach((element)=>{
    //element = tag a ในแต่ละตัว

    //ถ้า tag a ตัวไหนถูกคลิก
    element.addEventListener('click',(e)=>{
      console.log("Hello")

      // div class = submenu collapse  
      let childEl = element.nextElementSibling;

      // li 
      let parentEl = element.parentElement;
      
      //ถ้ามีตัวลูก (มี div)
      if(childEl){
        e.preventDefault()
        let mycollapse = new bootstrap.Collapse(childEl);
        if(childEl.classList.contains('show')){
          //ถ้าchild มี class = show อยู่แล้ว
          //ให้ hide ไป
          mycollapse.hide();
        }else{
          //ถ้าchild  ไม่มี class = show แต่จริงๆ show ตั้งแต่บรรทัดที่ 31 แล้ว 
          mycollapse.show();

          //หา other submenus [li] ที่มี class = show (กำลังเปิดอยู่) ให้ทำการปิดซะ ! เพราะเดียวเปิดเยอะเกิ้นน 
          closeOtherSubmenu();
        }
        //rotate ปุ่ม
        $(childEl).siblings('a').toggleClass('rotate')
        //active li
        $(parentEl).toggleClass('active')
      }

      function closeOtherSubmenu() {
        //parentEl = li
        //parentEl.parentElement = ul
        var opened_submenu=$(parentEl).siblings('li.has-submenu').children('.submenu.show')[0]
        //rotate ปุ่ม
        $(parentEl).siblings('li.has-submenu').children('.submenu.show').siblings('a').toggleClass('rotate')
        //ปิด active class
        $(parentEl).siblings('li.has-submenu.active').toggleClass('active')
        if (opened_submenu) {
          new bootstrap.Collapse(opened_submenu);
        }
      }
    })

  })



  //ปิด sidenav
  $('.close-btn').on('click',()=>{
    $('.sidebar').removeClass('show')
    //remove class openSidebar to  body
    $('body').removeClass('openSidebar')
  })

  //เปิด sidenav
  $(".navbar-toggler").on('click',()=>{
    $('.sidebar').addClass('show')
    //remove class openSidebar to  body
    $('body').addClass('openSidebar')
  })

}

//write event scroll
$('.sidebar-content').scroll(function() {
  console.log("Scroll on .sidebar")
  //check height of .navbar
  if ($('.sidebar-header').height() < $('.sidebar-content').scrollTop()) {
    // $('.sidebar-header').addClass('sticky-top')
  } else {
    // $('.sidebar-header').removeClass('sticky-top')
  }

});