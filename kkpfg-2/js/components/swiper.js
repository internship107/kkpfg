const swiper = new Swiper('.swiper-banner', {
  // Optional parameters
  loop: true,
  autoplay:{
    delay:3000,
    disableOnInteraction:false
  },

  
  breakpoints:{
    //>=0
    0:{
      pagination: {
            el: ".swiper-pagination",
            clickable:'true',
            type: "progressbar",
      }
    },
    //>=576
    576:{
     // If we need pagination
     pagination: {
      el: '.swiper-pagination',
      type:"bullets",
      renderBullet:(index,className)=>{
        return `<span class="${className}"><i></i><b></b></span>`
      }
      },
    }
  },
 
 
  // Navigation arrows
  navigation: {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev',
  },

  on:{
    init:function(){
      //set current index 
      var totalPages = $('.swiper-banner .swiper-wrapper .swiper-slide').length-2;
      var displayTotal;
      if(totalPages<10){
        displayTotal = "0"+totalPages;
      }
      $('#pagination-current').html("01")
      $('#pagination-total').html(displayTotal)
      
    },
    activeIndexChange:function(swiper){
      var displayCurrentIdx;
      if((swiper.realIndex+1)<10){
        displayCurrentIdx = "0"+(swiper.realIndex+1);
      }
      $('#pagination-current').html(displayCurrentIdx)
    },
    beforeResize:function(swiper){
      swiper.pagination.destroy()
    },
    resize:function(swiper){
      swiper.pagination.init()
      swiper.pagination.render()
      swiper.pagination.update()
    }
  }

});

var swiperCard = new Swiper(".swiper-card-expand-mobile", {
  slidesPerView: 1.2,
  spaceBetween: 1,
  centeredSlides: true,
  // freeMode:true,
  // watchSlidesProgress: true,
  // watchSlidesVisibility: true,
  loop:true,
  on:{
    init:function(swiper){
      this.slideTo(2)
    }
  }
});
