/*
 * ATTENTION: The "eval" devtool has been used (maybe by default in mode: "development").
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
/******/ (() => { // webpackBootstrap
/******/ 	var __webpack_modules__ = ({

/***/ "./js/components/cardExpand.js":
/*!*************************************!*\
  !*** ./js/components/cardExpand.js ***!
  \*************************************/
/***/ (() => {

eval("const items = document.querySelectorAll('.card-expand-desktop .card-image')\n\n  items.forEach(item => {\n    item.addEventListener('mouseover', () => {\n      removeActive()\n      console.log('active')\n      item.classList.add('active')\n    })\n  })\n\n  function removeActive() {\n    items.forEach(item => {\n      item.classList.remove('active')\n    })\n  }\n\n\n\n//# sourceURL=webpack://kkpfg-2/./js/components/cardExpand.js?");

/***/ }),

/***/ "./js/components/dropdown_nav.js":
/*!***************************************!*\
  !*** ./js/components/dropdown_nav.js ***!
  \***************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"renderDropDown\": () => (/* binding */ renderDropDown),\n/* harmony export */   \"setDropdownMenuPosition\": () => (/* binding */ setDropdownMenuPosition),\n/* harmony export */   \"setDropdownToggleHover\": () => (/* binding */ setDropdownToggleHover),\n/* harmony export */   \"stopPropagationDropdown\": () => (/* binding */ stopPropagationDropdown)\n/* harmony export */ });\n\n\n  //Task: ไม่ปิดถ้ากด dropdown menu (ul)\n  function stopPropagationDropdown(){\n\n  $('.navbar .dropdown-menu').on('click', function (e) {\n    console.log('Hello dropdown')\n    e.stopPropagation();\n  });\n}\n\nfunction renderDropDown(){\n  //Task:  Side bar เปิด-ปิดปุ่ม Sub-menu\n  document.querySelectorAll('.navbar .step2 a.dropdown-toggle').forEach((element)=>{\n  //element = tag a ในแต่ละตัว\n\n  //ถ้า tag a ตัวไหนถูกคลิก\n  element.addEventListener('click',(e)=>{\n    console.log(\"Hello\")\n\n    // div class = submenu collapse  \n    let childEl = element.nextElementSibling;\n\n    // li \n    let parentEl = element.parentElement;\n    \n    //ถ้ามีตัวลูก (มี div)\n    if(childEl){\n      e.preventDefault()\n      let mycollapse = new bootstrap.Collapse(childEl);\n      if(childEl.classList.contains('show')){\n        //ถ้าchild มี class = show อยู่แล้ว\n        //ให้ hide ไป\n        mycollapse.hide();\n      }else{\n        //ถ้าchild  ไม่มี class = show แต่จริงๆ show ตั้งแต่บรรทัดที่ 31 แล้ว \n        mycollapse.show();\n\n        //หา other submenus [li] ที่มี class = show (กำลังเปิดอยู่) ให้ทำการปิดซะ ! เพราะเดียวเปิดเยอะเกิ้นน \n        closeOtherSubmenu();\n      }\n      //rotate ปุ่ม\n      $(childEl).siblings('a').toggleClass('rotate')\n      //active li\n      $(parentEl).toggleClass('active')\n    }\n\n    function closeOtherSubmenu() {\n      //parentEl = li\n      //parentEl.parentElement = ul\n      var opened_submenu=$(parentEl).siblings('li.has-submenu').children('.submenu.show')[0]\n      //rotate ปุ่ม\n      $(parentEl).siblings('li.has-submenu').children('.submenu.show').siblings('a').toggleClass('rotate')\n      //ปิด active class\n      $(parentEl).siblings('li.has-submenu.active').toggleClass('active')\n      if (opened_submenu) {\n        new bootstrap.Collapse(opened_submenu);\n      }\n    }\n  })\n\n})\n}\n\n//set .dropdown-menu css to top equal to .navbar-expand-xl height\nfunction setDropdownMenuPosition(){\n  console.log('setDropdownMenuPosition')\n    document.querySelectorAll('.dropdown-menu').forEach((element)=> {\n    element.style.marginTop = 13 + 'px';\n    }\n  )\n}\n\n//set .dropdown-toggle from onclick to hover and will open .dropdown-menu\nfunction setDropdownToggleHover(){\n  $(\"#headerMenu\").children(\".nav-item\").hover(function() {\n    $('.step2.show').removeClass('show');\n  }, function() {\n  });\n\n  $('#headerMenu').children('.dropdown').hover(function() {\n      //on mouse over will remove other #headerMenu .dropdown-menu.show \n      $('#headerMenu .dropdown .step2.show').not('.step2 .dropdown').removeClass('show');\n\n      $(this).children('.step2').addClass('show');\n  }, function() {\n      if (!$(this).is(':hover') && !$(this).children('ul').is(':hover') && !$(this).closest('.navbar').is(':hover')) {\n        $(this).children('ul').removeClass('show');\n      }\n\n    \n  });\n\n};\n\n\n\n  \n\n\n\n\n\n//# sourceURL=webpack://kkpfg-2/./js/components/dropdown_nav.js?");

/***/ }),

/***/ "./js/components/header.js":
/*!*********************************!*\
  !*** ./js/components/header.js ***!
  \*********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"hoverNavLink\": () => (/* binding */ hoverNavLink)\n/* harmony export */ });\n//write event resize\r\n$(window).resize(function() { \r\n\r\n  //removeClass show at class sidebar\r\n  $('.sidebar').removeClass('show')\r\n  //removeClass openSidebar at body\r\n  $('body').removeClass('openSidebar')\r\n\r\n});\r\n\r\n\r\n//write event scroll\r\n$(window).scroll(function() {\r\n  //check height of .navbar\r\n  if ($('.navbar').height() < $(window).scrollTop()) {\r\n    $('.navbar').addClass('fixed-top')\r\n  } else {\r\n    $('.navbar').removeClass('fixed-top')\r\n  }\r\n\r\n});\r\n\r\n\r\n//if hover children #headerMenu > .nav-link will add class active\r\n\r\nfunction hoverNavLink(){\r\n  $(\"#headerMenu\").children(\".nav-item\").hover(function() {\r\n    // $(this).children(\".nav-link\").addClass(\"active\");\r\n  }, function() {\r\n    // $(this).children(\".nav-link\").removeClass(\"active\");\r\n  });\r\n}\r\n\r\n\n\n//# sourceURL=webpack://kkpfg-2/./js/components/header.js?");

/***/ }),

/***/ "./js/components/sidebar_nav.js":
/*!**************************************!*\
  !*** ./js/components/sidebar_nav.js ***!
  \**************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"controlSideNav\": () => (/* binding */ controlSideNav)\n/* harmony export */ });\nfunction controlSideNav(){\n  //Task:  Side bar เปิด-ปิดปุ่ม Sub-menu\n  document.querySelectorAll('.sidebar .nav-link').forEach((element)=>{\n    //element = tag a ในแต่ละตัว\n\n    //ถ้า tag a ตัวไหนถูกคลิก\n    element.addEventListener('click',(e)=>{\n      console.log(\"Hello\")\n\n      // div class = submenu collapse  \n      let childEl = element.nextElementSibling;\n\n      // li \n      let parentEl = element.parentElement;\n      \n      //ถ้ามีตัวลูก (มี div)\n      if(childEl){\n        e.preventDefault()\n        let mycollapse = new bootstrap.Collapse(childEl);\n        if(childEl.classList.contains('show')){\n          //ถ้าchild มี class = show อยู่แล้ว\n          //ให้ hide ไป\n          mycollapse.hide();\n        }else{\n          //ถ้าchild  ไม่มี class = show แต่จริงๆ show ตั้งแต่บรรทัดที่ 31 แล้ว \n          mycollapse.show();\n\n          //หา other submenus [li] ที่มี class = show (กำลังเปิดอยู่) ให้ทำการปิดซะ ! เพราะเดียวเปิดเยอะเกิ้นน \n          closeOtherSubmenu();\n        }\n        //rotate ปุ่ม\n        $(childEl).siblings('a').toggleClass('rotate')\n        //active li\n        $(parentEl).toggleClass('active')\n      }\n\n      function closeOtherSubmenu() {\n        //parentEl = li\n        //parentEl.parentElement = ul\n        var opened_submenu=$(parentEl).siblings('li.has-submenu').children('.submenu.show')[0]\n        //rotate ปุ่ม\n        $(parentEl).siblings('li.has-submenu').children('.submenu.show').siblings('a').toggleClass('rotate')\n        //ปิด active class\n        $(parentEl).siblings('li.has-submenu.active').toggleClass('active')\n        if (opened_submenu) {\n          new bootstrap.Collapse(opened_submenu);\n        }\n      }\n    })\n\n  })\n\n\n\n  //ปิด sidenav\n  $('.close-btn').on('click',()=>{\n    $('.sidebar').removeClass('show')\n    //remove class openSidebar to  body\n    $('body').removeClass('openSidebar')\n  })\n\n  //เปิด sidenav\n  $(\".navbar-toggler\").on('click',()=>{\n    $('.sidebar').addClass('show')\n    //remove class openSidebar to  body\n    $('body').addClass('openSidebar')\n  })\n\n}\n\n//write event scroll\n$('.sidebar-content').scroll(function() {\n  console.log(\"Scroll on .sidebar\")\n  //check height of .navbar\n  if ($('.sidebar-header').height() < $('.sidebar-content').scrollTop()) {\n    // $('.sidebar-header').addClass('sticky-top')\n  } else {\n    // $('.sidebar-header').removeClass('sticky-top')\n  }\n\n});\n\n//# sourceURL=webpack://kkpfg-2/./js/components/sidebar_nav.js?");

/***/ }),

/***/ "./js/components/swiper.js":
/*!*********************************!*\
  !*** ./js/components/swiper.js ***!
  \*********************************/
/***/ (() => {

eval("const swiper = new Swiper('.swiper-banner', {\n  // Optional parameters\n  loop: true,\n  autoplay:{\n    delay:3000,\n    disableOnInteraction:false\n  },\n\n  \n  breakpoints:{\n    //>=0\n    0:{\n      pagination: {\n            el: \".swiper-pagination\",\n            clickable:'true',\n            type: \"progressbar\",\n      }\n    },\n    //>=576\n    576:{\n     // If we need pagination\n     pagination: {\n      el: '.swiper-pagination',\n      type:\"bullets\",\n      renderBullet:(index,className)=>{\n        return `<span class=\"${className}\"><i></i><b></b></span>`\n      }\n      },\n    }\n  },\n \n \n  // Navigation arrows\n  navigation: {\n    nextEl: '.swiper-button-next',\n    prevEl: '.swiper-button-prev',\n  },\n\n  on:{\n    init:function(){\n      //set current index \n      var totalPages = $('.swiper-banner .swiper-wrapper .swiper-slide').length-2;\n      var displayTotal;\n      if(totalPages<10){\n        displayTotal = \"0\"+totalPages;\n      }\n      $('#pagination-current').html(\"01\")\n      $('#pagination-total').html(displayTotal)\n      \n    },\n    activeIndexChange:function(swiper){\n      var displayCurrentIdx;\n      if((swiper.realIndex+1)<10){\n        displayCurrentIdx = \"0\"+(swiper.realIndex+1);\n      }\n      $('#pagination-current').html(displayCurrentIdx)\n    },\n    beforeResize:function(swiper){\n      swiper.pagination.destroy()\n    },\n    resize:function(swiper){\n      swiper.pagination.init()\n      swiper.pagination.render()\n      swiper.pagination.update()\n    }\n  }\n\n});\n\nvar swiperCard = new Swiper(\".swiper-card-expand-mobile\", {\n  slidesPerView: 1.2,\n  spaceBetween: 1,\n  centeredSlides: true,\n  // freeMode:true,\n  // watchSlidesProgress: true,\n  // watchSlidesVisibility: true,\n  loop:true,\n  on:{\n    init:function(swiper){\n      this.slideTo(2)\n    }\n  }\n});\n\n\n//# sourceURL=webpack://kkpfg-2/./js/components/swiper.js?");

/***/ }),

/***/ "./js/data/header-footer.js":
/*!**********************************!*\
  !*** ./js/data/header-footer.js ***!
  \**********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"fetchData\": () => (/* binding */ fetchData)\n/* harmony export */ });\nfunction fetchData(){\n  const url=\"https://demowcmws.clicknext.net/api/v1/website/th/4?MenuCodes=o3cy6mgrf2h49gi2cr45k036mbz1gmlz\"\n  const data = fetch(url)           //api for the get request\n  .then(response => response.json())\n  .then(data => data);\n  return data;\n}\n\n\n\n\n\n\n//# sourceURL=webpack://kkpfg-2/./js/data/header-footer.js?");

/***/ }),

/***/ "./js/render/header-footer.js":
/*!************************************!*\
  !*** ./js/render/header-footer.js ***!
  \************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"renderData\": () => (/* binding */ renderData)\n/* harmony export */ });\n/* harmony import */ var _data_header_footer_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../data/header-footer.js */ \"./js/data/header-footer.js\");\n\n\nasync function renderData(){\n\n  const data = await (0,_data_header_footer_js__WEBPACK_IMPORTED_MODULE_0__.fetchData)();\n  $(\"#headerMenu\").html(\"\")\n  \n  // $(\"#headerMenu\").html(\"\")\n  // const headerData = data.Data.allMenuSet[0]?.menuSet\n  // await headerData.forEach((menu,index)=>{\n    \n  //   //!lv1\n  //   $(\"#headerMenu\").append(`\n  //   <li class=\"nav-item ${menu.menuChilds.length >0 ? \"dropdown\" : \"\" }\" id=\"${index}-li\">\n  //     <a class=\"nav-link  ${menu.menuChilds.length >0 ? \"dropdown-toggle\" : \"\" } \" id=\"${index}-a\" href=\"#\" role=\"button\" data-bs-toggle=\"dropdown\" aria-expanded=\"false\">${menu.menuText}</a>\n  //   </li>\n  //   `);\n  //       //!lv2\n  //       const menuLv2 = menu.menuChilds;\n  //       if(menuLv2.length>0){\n  //         const $ulLv2 = $(`<ul class=\"dropdown-menu step2\" id=\"${index}-ul\" aria-labelledby=\"${index}-a\"></ul>`) \n  //         menuLv2.forEach((menu2,index2)=>{\n  //           $($ulLv2).append(`\n  //               <li id=\"${index2}-li\" class=\"${menu2.menuChilds.length>0? \"dropdown\":\"\"} dropdown mega-dropdown\"><a id=\"${index2}-a\" class=\"dropdown-item ${menu2.menuChilds.length >0 ? \"dropdown-toggle\" : \"\" }\">${menu2.menuText}</a></li>\n  //           `)\n            \n\n  //             //!lv3\n  //             const menuLv3 = menu2.menuChilds;\n  //             if(menuLv3.length>0){\n  //               const $ulLv3 = $(`<ul class=\"navbar-nav step3\" id=\"${index2}-ul\" aria-labelledby=\"${index2}-a\"></ul>`) \n  //               menuLv3.forEach(menu3=>{\n  //                 $($ulLv3).append(`\n  //                   <li><a class=\"dropdown-item\" href=\"#\">${menu3.menuText}</a></li>\n  //                 `)\n  //               })\n\n  //               const $ulLv3withContainer = $(`<div class=\"submenu collapse\"></div>`)\n  //               $ulLv3withContainer.append($ulLv3)\n  //               //append lv3 \n  //               $ulLv2.children(`li#${index2}-li`).append($ulLv3withContainer)\n  //             }\n  //             //end lv3\n\n\n  //         })\n  //         //append lv2\n  //         $(`#${index}-li`).append($ulLv2)\n  //         //end lv2\n  //       }\n            \n  // })\n\n  const renderHeaderMenu = (data) => {\n    //get menuSet\n    const menuSet = data.Data.allMenuSet[0]?.menuSet\n\n    //loop menuSet\n    menuSet.forEach((menu, index) => {\n        //create li\n        const $li = $('<li>', {\n            class: `${menu.menuChilds.length > 0 ? \"dropdown\" : \"\"} nav-item`,\n            id: `${index}-li`,\n        });\n\n        //create a\n        const $a = $('<a>', {\n            class: `${menu.menuChilds.length > 0 ? \"dropdown-toggle\" : \"\"} nav-link`,\n            id: `${index}-a`,\n            href: '#',\n            role: 'button',\n            'data-bs-toggle': 'dropdown',\n            'aria-expanded': 'false',\n            text:menu.menuText\n        });\n\n        //append a to li\n        $li.append($a);\n\n        \n        //check menuChilds => lv2\n        if (menu.menuChilds.length > 0) {\n            //create ul\n            const $ul = $('<ul>', {\n                class: 'dropdown-menu step2',\n                id: `${index}-ul`,\n                'aria-labelledby': `${index}-a`\n            });\n\n            //loop menuChilds\n            menu.menuChilds.forEach((menuChild, menuChildIndex) => {\n                //create li\n                const $menuChildLi = $('<li>', {\n                    class: menuChild.menuChilds.length > 0 ? \"dropdown\" : \"\",\n                    id: `${menuChildIndex}-li`\n                });\n\n                //create a\n                const $menuChildA = $('<a>', {\n                    class: `${menuChild.menuChilds.length > 0 ? \"dropdown-toggle\" : \"\"} dropdown-item`,\n                    id: `${menuChildIndex}-a`,\n                    href: '#',\n                    text: menuChild.menuText\n                });\n\n                //append a to li\n                $menuChildLi.append($menuChildA);\n\n                //check menuChilds =>lv3\n                if (menuChild.menuChilds.length > 0) {\n                    //create ul\n                    const $menuChildUl = $('<ul>', {\n                        class: 'navbar-nav step3',\n                        id: `${menuChildIndex}-ul`,\n                        'aria-labelledby': `${menuChildIndex}-a`\n                    });\n\n                    //loop menuChilds\n                    menuChild.menuChilds.forEach((menuChildChild, menuChildChildIndex) => {\n                        //create li\n                        const $menuChildChildLi = $('<li>');\n\n                        //create a\n                        const $menuChildChildA = $('<a>', {\n                            class: 'dropdown-item',\n                            href: '#',\n                            text: menuChildChild.menuText\n                        });\n\n                        //append a to li\n                        $menuChildChildLi.append($menuChildChildA);\n\n                        //append li to ul\n                        $menuChildUl.append($menuChildChildLi);\n                    });\n\n                    //create div\n                    const $collapseContainer = $('<div>', {\n                        class: 'submenu collapse'\n                    });\n\n                    //append ul to div\n                    // $menuChildUl.wrap($collapseContainer)\n                    $collapseContainer.append($menuChildUl);\n\n                    //append div to li\n                    $menuChildLi.append($collapseContainer);\n                }\n\n                //append li to ul\n                $ul.append($menuChildLi);\n            });\n\n            //append ul to li\n            $li.append($ul);\n        }\n\n        //append li to DOM\n        $('#headerMenu').append($li);\n    });\n  }\n\n  renderHeaderMenu(data);\n\n\n  //footer\n  const tel = data.Data.tel;\n  const copyRight = data.Data.copyRight;\n  $(\"#footer-Tel\").removeClass(function(index, className) {\n    return (className.match(/\\bskeleton\\S*/g) || []).join(' ');\n  });\n  $(\"#footer-Tel\").html(`${tel}`)\n  $(\"#footer-CopyRight\").removeClass(function(index, className) {\n    return (className.match(/\\bskeleton\\S*/g) || []).join(' ');\n  });\n  $(\"#footer-CopyRight\").html(`${copyRight}`)\n\n\n  //sideBar\n\n}\n\n\n\n\n//# sourceURL=webpack://kkpfg-2/./js/render/header-footer.js?");

/***/ }),

/***/ "./js/render/renderAll.js":
/*!********************************!*\
  !*** ./js/render/renderAll.js ***!
  \********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _data_header_footer__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../data/header-footer */ \"./js/data/header-footer.js\");\n/* harmony import */ var _components_dropdown_nav__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../components/dropdown_nav */ \"./js/components/dropdown_nav.js\");\n/* harmony import */ var _render_sidebar__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../render/sidebar */ \"./js/render/sidebar.js\");\n/* harmony import */ var _components_sidebar_nav__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../components/sidebar_nav */ \"./js/components/sidebar_nav.js\");\n/* harmony import */ var _components_header__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../components/header */ \"./js/components/header.js\");\n/* harmony import */ var _render_header_footer__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../render/header-footer */ \"./js/render/header-footer.js\");\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\nasync function renderAll(){\r\n  await (0,_render_header_footer__WEBPACK_IMPORTED_MODULE_5__.renderData)()\r\n  ;(0,_components_dropdown_nav__WEBPACK_IMPORTED_MODULE_1__.renderDropDown)()\r\n  ;(0,_components_dropdown_nav__WEBPACK_IMPORTED_MODULE_1__.stopPropagationDropdown)()\r\n  await (0,_render_sidebar__WEBPACK_IMPORTED_MODULE_2__.renderSideNav)()\r\n  ;(0,_components_sidebar_nav__WEBPACK_IMPORTED_MODULE_3__.controlSideNav)()\r\n  ;(0,_components_dropdown_nav__WEBPACK_IMPORTED_MODULE_1__.setDropdownMenuPosition)()\r\n  ;(0,_components_dropdown_nav__WEBPACK_IMPORTED_MODULE_1__.setDropdownToggleHover)()\r\n  ;(0,_components_header__WEBPACK_IMPORTED_MODULE_4__.hoverNavLink)()\r\n} \r\n\r\nrenderAll()\n\n//# sourceURL=webpack://kkpfg-2/./js/render/renderAll.js?");

/***/ }),

/***/ "./js/render/sidebar.js":
/*!******************************!*\
  !*** ./js/render/sidebar.js ***!
  \******************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"renderSideNav\": () => (/* binding */ renderSideNav)\n/* harmony export */ });\n/* harmony import */ var _data_header_footer__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../data/header-footer */ \"./js/data/header-footer.js\");\n\n\nasync function renderSideNav(){\n  const data = await (0,_data_header_footer__WEBPACK_IMPORTED_MODULE_0__.fetchData)();\n\n  //sidebar\n  $(\"#sidebarMenu\").html(\"\")\n\n\n  const sidebarData = data.Data.allMenuSet[0]?.menuSet\n  await sidebarData.forEach((menu,index)=>{\n    //have multi level\n    if(menu.menuChilds.length>0){\n      //=>lv2\n        const $liLv1 = $(`\n          <li class=\"nav-item has-submenu\">\n              <a href=\"#\" class=\"nav-link dropdown-toggle lv1\">${menu.menuText}</a>\n          </li>\n        `)\n        //!lv2\n        const menuLv2 = menu.menuChilds;\n        const $ulLv2 = $(`<ul class=\"navbar-nav step2\"></ul>`)\n        menuLv2.forEach((menu2,index2)=>{\n          if(menu2.menuChilds.length>0){\n            //thrid level\n            const $liLv2 = $(`<li class=\"nav-item has-submenu\">\n            <a class=\"nav-link dropdown-toggle\">${menu2.menuText}</a>\n            </li>`)\n\n              //!lv3\n              const menuLv3 = menu2.menuChilds;\n              const $ulLv3 = $(`<ul class=\"navbar-nav step3\"></ul>`)\n              if(menuLv3.length>0){\n                menuLv3.forEach(menu3=>{\n                  $($ulLv3).append(`\n                  <li class=\"nav-item\">\n                        <a href=\"#\" class=\"nav-link\">${menu3.menuText}</a>\n                  </li>\n                  `)\n                }) //loop\n\n                //wrap with collapse container\n                const $ulLv3withContainer = $(`<div class=\"submenu collapse\"></div>`)\n                $ulLv3withContainer.append($ulLv3)\n                //li add container collapse\n                $liLv2.append($ulLv3withContainer)\n                $ulLv2.append($liLv2)\n              }\n\n\n          }else{\n            // console.log(menu2.menuText)\n                //end at second level\n                const $liLv2 = $(`<li class=\"nav-item\">\n                <a class=\"nav-link\">${menu2.menuText}</a>\n                </li>`)\n                $ulLv2.append($liLv2)\n          }\n        }) //loop\n\n        //!lv1 append container 2\n        //wrap with collapse container\n        const $ulLv2withContainer = $(`<div class=\"submenu collapse\"></div>`)\n        $ulLv2withContainer.append($ulLv2)\n        //append to liLv1\n        $liLv1.append($ulLv2withContainer)\n        $(\"#sidebarMenu\").append($liLv1)\n        \n\n    }else{\n      //one level\n      //? lv1\n      const $liLv1 = (`<li class=\"nav-item\">\n                      <a href=\"\" class=\"nav-link\">${menu.menuText}</a>\n                    </li>`)\n      $(\"#sidebarMenu\").append($liLv1)\n    }\n  })\n\n  \n  \n\n}\n\n\n\n//# sourceURL=webpack://kkpfg-2/./js/render/sidebar.js?");

/***/ }),

/***/ "./js/script.js":
/*!**********************!*\
  !*** ./js/script.js ***!
  \**********************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _render_renderAll__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./render/renderAll */ \"./js/render/renderAll.js\");\n/* harmony import */ var _components_cardExpand__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./components/cardExpand */ \"./js/components/cardExpand.js\");\n/* harmony import */ var _components_cardExpand__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_components_cardExpand__WEBPACK_IMPORTED_MODULE_1__);\n/* harmony import */ var _components_swiper__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./components/swiper */ \"./js/components/swiper.js\");\n/* harmony import */ var _components_swiper__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_components_swiper__WEBPACK_IMPORTED_MODULE_2__);\n/* harmony import */ var _components_sidebar_nav__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./components/sidebar_nav */ \"./js/components/sidebar_nav.js\");\n/* harmony import */ var _components_header__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./components/header */ \"./js/components/header.js\");\n//render\n\n\n\n//component\n\n\n\n\n\n\n\n\n//# sourceURL=webpack://kkpfg-2/./js/script.js?");

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/compat get default export */
/******/ 	(() => {
/******/ 		// getDefaultExport function for compatibility with non-harmony modules
/******/ 		__webpack_require__.n = (module) => {
/******/ 			var getter = module && module.__esModule ?
/******/ 				() => (module['default']) :
/******/ 				() => (module);
/******/ 			__webpack_require__.d(getter, { a: getter });
/******/ 			return getter;
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/define property getters */
/******/ 	(() => {
/******/ 		// define getter functions for harmony exports
/******/ 		__webpack_require__.d = (exports, definition) => {
/******/ 			for(var key in definition) {
/******/ 				if(__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) {
/******/ 					Object.defineProperty(exports, key, { enumerable: true, get: definition[key] });
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	(() => {
/******/ 		__webpack_require__.o = (obj, prop) => (Object.prototype.hasOwnProperty.call(obj, prop))
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	(() => {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = (exports) => {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	})();
/******/ 	
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module can't be inlined because the eval devtool is used.
/******/ 	var __webpack_exports__ = __webpack_require__("./js/script.js");
/******/ 	
/******/ })()
;