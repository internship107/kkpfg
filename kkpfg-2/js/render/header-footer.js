import { fetchData } from "../data/header-footer.js";

export async function renderData(){

  const data = await fetchData();
  $("#headerMenu").html("")
  
  // $("#headerMenu").html("")
  // const headerData = data.Data.allMenuSet[0]?.menuSet
  // await headerData.forEach((menu,index)=>{
    
  //   //!lv1
  //   $("#headerMenu").append(`
  //   <li class="nav-item ${menu.menuChilds.length >0 ? "dropdown" : "" }" id="${index}-li">
  //     <a class="nav-link  ${menu.menuChilds.length >0 ? "dropdown-toggle" : "" } " id="${index}-a" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">${menu.menuText}</a>
  //   </li>
  //   `);
  //       //!lv2
  //       const menuLv2 = menu.menuChilds;
  //       if(menuLv2.length>0){
  //         const $ulLv2 = $(`<ul class="dropdown-menu step2" id="${index}-ul" aria-labelledby="${index}-a"></ul>`) 
  //         menuLv2.forEach((menu2,index2)=>{
  //           $($ulLv2).append(`
  //               <li id="${index2}-li" class="${menu2.menuChilds.length>0? "dropdown":""} dropdown mega-dropdown"><a id="${index2}-a" class="dropdown-item ${menu2.menuChilds.length >0 ? "dropdown-toggle" : "" }">${menu2.menuText}</a></li>
  //           `)
            

  //             //!lv3
  //             const menuLv3 = menu2.menuChilds;
  //             if(menuLv3.length>0){
  //               const $ulLv3 = $(`<ul class="navbar-nav step3" id="${index2}-ul" aria-labelledby="${index2}-a"></ul>`) 
  //               menuLv3.forEach(menu3=>{
  //                 $($ulLv3).append(`
  //                   <li><a class="dropdown-item" href="#">${menu3.menuText}</a></li>
  //                 `)
  //               })

  //               const $ulLv3withContainer = $(`<div class="submenu collapse"></div>`)
  //               $ulLv3withContainer.append($ulLv3)
  //               //append lv3 
  //               $ulLv2.children(`li#${index2}-li`).append($ulLv3withContainer)
  //             }
  //             //end lv3


  //         })
  //         //append lv2
  //         $(`#${index}-li`).append($ulLv2)
  //         //end lv2
  //       }
            
  // })

  const renderHeaderMenu = (data) => {
    //get menuSet
    const menuSet = data.Data.allMenuSet[0]?.menuSet

    //loop menuSet
    menuSet.forEach((menu, index) => {
        //create li
        const $li = $('<li>', {
            class: `${menu.menuChilds.length > 0 ? "dropdown" : ""} nav-item`,
            id: `${index}-li`,
        });

        //create a
        const $a = $('<a>', {
            class: `${menu.menuChilds.length > 0 ? "dropdown-toggle" : ""} nav-link`,
            id: `${index}-a`,
            href: '#',
            role: 'button',
            'data-bs-toggle': 'dropdown',
            'aria-expanded': 'false',
            text:menu.menuText
        });

        //append a to li
        $li.append($a);

        
        //check menuChilds => lv2
        if (menu.menuChilds.length > 0) {
            //create ul
            const $ul = $('<ul>', {
                class: 'dropdown-menu step2',
                id: `${index}-ul`,
                'aria-labelledby': `${index}-a`
            });

            //loop menuChilds
            menu.menuChilds.forEach((menuChild, menuChildIndex) => {
                //create li
                const $menuChildLi = $('<li>', {
                    class: menuChild.menuChilds.length > 0 ? "dropdown" : "",
                    id: `${menuChildIndex}-li`
                });

                //create a
                const $menuChildA = $('<a>', {
                    class: `${menuChild.menuChilds.length > 0 ? "dropdown-toggle" : ""} dropdown-item`,
                    id: `${menuChildIndex}-a`,
                    href: '#',
                    text: menuChild.menuText
                });

                //append a to li
                $menuChildLi.append($menuChildA);

                //check menuChilds =>lv3
                if (menuChild.menuChilds.length > 0) {
                    //create ul
                    const $menuChildUl = $('<ul>', {
                        class: 'navbar-nav step3',
                        id: `${menuChildIndex}-ul`,
                        'aria-labelledby': `${menuChildIndex}-a`
                    });

                    //loop menuChilds
                    menuChild.menuChilds.forEach((menuChildChild, menuChildChildIndex) => {
                        //create li
                        const $menuChildChildLi = $('<li>');

                        //create a
                        const $menuChildChildA = $('<a>', {
                            class: 'dropdown-item',
                            href: '#',
                            text: menuChildChild.menuText
                        });

                        //append a to li
                        $menuChildChildLi.append($menuChildChildA);

                        //append li to ul
                        $menuChildUl.append($menuChildChildLi);
                    });

                    //create div
                    const $collapseContainer = $('<div>', {
                        class: 'submenu collapse'
                    });

                    //append ul to div
                    // $menuChildUl.wrap($collapseContainer)
                    $collapseContainer.append($menuChildUl);

                    //append div to li
                    $menuChildLi.append($collapseContainer);
                }

                //append li to ul
                $ul.append($menuChildLi);
            });

            //append ul to li
            $li.append($ul);
        }

        //append li to DOM
        $('#headerMenu').append($li);
    });
  }

  renderHeaderMenu(data);


  //footer
  const tel = data.Data.tel;
  const copyRight = data.Data.copyRight;
  $("#footer-Tel").removeClass(function(index, className) {
    return (className.match(/\bskeleton\S*/g) || []).join(' ');
  });
  $("#footer-Tel").html(`${tel}`)
  $("#footer-CopyRight").removeClass(function(index, className) {
    return (className.match(/\bskeleton\S*/g) || []).join(' ');
  });
  $("#footer-CopyRight").html(`${copyRight}`)


  //sideBar

}


