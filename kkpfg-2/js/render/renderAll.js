import { fetchData } from '../data/header-footer'
import { renderDropDown , stopPropagationDropdown ,setDropdownMenuPosition ,setDropdownToggleHover} from '../components/dropdown_nav';
import {renderSideNav} from '../render/sidebar'
import {controlSideNav} from '../components/sidebar_nav'
import {hoverNavLink} from '../components/header'
import {renderData as renderDataHeaderFooter} from '../render/header-footer'



async function renderAll(){
  await renderDataHeaderFooter()
  renderDropDown()
  stopPropagationDropdown()
  await renderSideNav()
  controlSideNav()
  setDropdownMenuPosition()
  setDropdownToggleHover()
  hoverNavLink()
} 

renderAll()