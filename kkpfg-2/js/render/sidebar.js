import { fetchData } from "../data/header-footer";

async function renderSideNav(){
  const data = await fetchData();

  //sidebar
  $("#sidebarMenu").html("")


  const sidebarData = data.Data.allMenuSet[0]?.menuSet
  await sidebarData.forEach((menu,index)=>{
    //have multi level
    if(menu.menuChilds.length>0){
      //=>lv2
        const $liLv1 = $(`
          <li class="nav-item has-submenu">
              <a href="#" class="nav-link dropdown-toggle lv1">${menu.menuText}</a>
          </li>
        `)
        //!lv2
        const menuLv2 = menu.menuChilds;
        const $ulLv2 = $(`<ul class="navbar-nav step2"></ul>`)
        menuLv2.forEach((menu2,index2)=>{
          if(menu2.menuChilds.length>0){
            //thrid level
            const $liLv2 = $(`<li class="nav-item has-submenu">
            <a class="nav-link dropdown-toggle">${menu2.menuText}</a>
            </li>`)

              //!lv3
              const menuLv3 = menu2.menuChilds;
              const $ulLv3 = $(`<ul class="navbar-nav step3"></ul>`)
              if(menuLv3.length>0){
                menuLv3.forEach(menu3=>{
                  $($ulLv3).append(`
                  <li class="nav-item">
                        <a href="#" class="nav-link">${menu3.menuText}</a>
                  </li>
                  `)
                }) //loop

                //wrap with collapse container
                const $ulLv3withContainer = $(`<div class="submenu collapse"></div>`)
                $ulLv3withContainer.append($ulLv3)
                //li add container collapse
                $liLv2.append($ulLv3withContainer)
                $ulLv2.append($liLv2)
              }


          }else{
            // console.log(menu2.menuText)
                //end at second level
                const $liLv2 = $(`<li class="nav-item">
                <a class="nav-link">${menu2.menuText}</a>
                </li>`)
                $ulLv2.append($liLv2)
          }
        }) //loop

        //!lv1 append container 2
        //wrap with collapse container
        const $ulLv2withContainer = $(`<div class="submenu collapse"></div>`)
        $ulLv2withContainer.append($ulLv2)
        //append to liLv1
        $liLv1.append($ulLv2withContainer)
        $("#sidebarMenu").append($liLv1)
        

    }else{
      //one level
      //? lv1
      const $liLv1 = (`<li class="nav-item">
                      <a href="" class="nav-link">${menu.menuText}</a>
                    </li>`)
      $("#sidebarMenu").append($liLv1)
    }
  })

  
  

}

export {renderSideNav}